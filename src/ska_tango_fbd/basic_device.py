# Copyright (c) 2024 CSIRO
"""Basic Tango Device for demonstration purposes."""
# pylint: disable=attribute-defined-outside-init

from tango.server import AttrWriteType, Device, attribute, run


class BasicDevice(Device):
    """A very basic Tango device."""

    def init_device(self):
        """Initialize."""
        super().init_device()
        self._a = 0
        self._b = 0

    def delete_device(self):
        """Destroy."""

    def always_executed_hook(self):
        """Do thing all time."""

    @attribute(access=AttrWriteType.READ_WRITE, dtype=int)
    def a(self) -> int:  # pylint: disable=invalid-name
        """Input Attribute A."""
        return self._a

    @a.write
    def a(self, value: int) -> None:  # pylint: disable=invalid-name
        """Set Attribute A."""
        self._a = value

    @attribute(access=AttrWriteType.READ_WRITE, dtype=int)
    def b(self) -> int:  # pylint: disable=invalid-name
        """Input Attribute B."""
        return self._b

    @b.write
    def b(self, value: int) -> None:  # pylint: disable=invalid-name
        """Set Attribute B."""
        self._b = value

    @attribute(dtype=int)
    def x(self) -> int:  # pylint: disable=invalid-name
        """Output Attribute X (=A)."""
        return self._a

    @attribute(dtype=int)
    def y(self) -> int:  # pylint: disable=invalid-name
        """Output Attribute Y (=A+B)."""
        return self._a + self._b


def main(args=None, **kwargs):
    """Launch device server."""
    return run((BasicDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
