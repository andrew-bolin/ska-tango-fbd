ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-pytango-builder:9.4.2"
ARG BASE_IMAGE="artefact.skao.int/ska-tango-images-pytango-runtime:9.4.2"

FROM $BUILD_IMAGE as buildenv
FROM $BASE_IMAGE

USER root

RUN poetry config virtualenvs.create false
RUN poetry install

USER tango

