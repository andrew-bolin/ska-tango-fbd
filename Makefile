PROJECT = ska-tango-fbd
KUBE_NAMESPACE ?= $(PROJECT)
RELEASE_NAME ?= test
K8S_CHART ?= test-parent
K8S_CHARTS = $(K8S_CHART)
HELM_CHARTS_TO_PUBLISH = $(PROJECT)
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test
#MINIKUBE ?= true
OCI_BUILDER ?= podman
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server

include .make/base.mk
include .make/python.mk
include .make/helm.mk
include .make/k8s.mk
include .make/oci.mk

DOCS_SPHINXOPTS = -n -W --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --no-root --only docs

.PHONY: docs-pre-build

PYTHON_LINE_LENGTH = 88

K8S_CHART_PARAMS = --set global.tango_host=$(TANGO_HOST) \
		--set global.device_server_port=$(TANGO_SERVER_PORT)
