# ska-tango-fbd

Tango Function Block Diagram (Innovation/Experiment!)


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-tango-fbd/badge/?version=latest)](https://developer.skao.int/projects/ska-tango-fbd/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-tango-fbd documentation](https://developer.skatelescope.org/projects/ska-tango-fbd/en/latest/index.html "SKA Developer Portal: ska-tango-fbd documentation")

## Features

* TODO
